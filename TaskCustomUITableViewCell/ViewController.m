//
//  ViewController.m
//  TaskCustomUITableViewCell
//
//  Created by Kos  on 21/08/15.
//  Copyright (c) 2015 Kos . All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
@interface ViewController ()




@end

@implementation ViewController


@synthesize companyArray, company;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *path = [[NSBundle mainBundle]pathForResource:@"company"  ofType:@"plist"];
    companyArray = [NSArray arrayWithContentsOfFile:path];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   static NSString *cellIdentifier = @"Cell";
    CustomTableViewCell *customCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    company = companyArray[indexPath.row];
    
    NSString *companyName = company[@"companyName"];
    NSString *imageName = company[@"imageName"];
    
    UIImage *image = [UIImage imageNamed:imageName];
    
    customCell.customCompanyNameLabel.text= companyName;
    customCell.customCompanyLogo.image = image;
    
    return customCell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [companyArray count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
