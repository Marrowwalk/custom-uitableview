//
//  CustomTableViewCell.h
//  TaskCustomUITableViewCell
//
//  Created by Kos  on 21/08/15.
//  Copyright (c) 2015 Kos . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *customCompanyNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *customCompanyLogo;

@end
